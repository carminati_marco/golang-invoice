package main

import (
  "calculator"
  "currencies"
	"database"
  "encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"os"
)


// util
func getFrontendUrl() string {
	if os.Getenv("APP_ENV") == "production" {
		return "http://localhost:3000" // change this to production domain
	} else {
		return "http://localhost:3000"
	}
}

// used for COR preflight checks
func corsHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setCors(w)
}

// used to set CORS.
func setCors(w http.ResponseWriter) {
	frontendUrl := getFrontendUrl()
	w.Header().Set("Access-Control-Allow-Origin", frontendUrl)
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS, POST, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
}

// home page => to see if REST works.
func indexHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setCors(w)
	fmt.Fprintf(w, "This is the RESTful api -YEAH!")
}

// create a new invoice.
func createInvoiceHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setCors(w)

  // get and decode
	decoder := json.NewDecoder(r.Body)
	var newInvoice database.Invoice
	if err := decoder.Decode(&newInvoice); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

  // check if invoice is valid.
  var validityStatus database.ValidityStatus
  validityStatus = newInvoice.IsValid()

  if validityStatus.Code != 200 {
    http.Error(w, validityStatus.Text, validityStatus.Code)
    return
  }

  // create Invoice
  database.DB.Create(&newInvoice)
	res, err := json.Marshal(newInvoice)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write(res)
}

// create a new Product
func createProductHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setCors(w)

  // get and decode
	decoder := json.NewDecoder(r.Body)
	var newProduct database.Product
	if err := decoder.Decode(&newProduct); err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

  // check if invoice is valid.
  var validityStatus database.ValidityStatus
  validityStatus = newProduct.IsValid()

  if validityStatus.Code != 200 {
    http.Error(w, validityStatus.Text, validityStatus.Code)
    return
  }

  // update data from invoice.
  newProduct.InvoiceID = int(validityStatus.Invoice.ID)
  newProduct.Invoice = validityStatus.Invoice

  // create Product
  database.DB.Create(&newProduct)

  // update Invoice and Product
  newProduct = calculator.AddProduct(newProduct)

  res, err := json.Marshal(newProduct)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write(res)
}

// delete product.
func deleteProductHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setCors(w)

  var product database.Product
	database.DB.Where("ID = ?", ps.ByName("productId")).First(&product)

  product = calculator.RemoveProduct(product)

  res, err := json.Marshal(database.Product{})
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	w.Write(res)
}

// get list of invoices.
func indexInvoiceHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	setCors(w)
	var invoices []database.Invoice
	database.DB.Find(&invoices)
	res, err := json.Marshal(invoices)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Write(res)
}

// get detail invoice -> filter by invoiceNumber.
func showInvoiceHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setCors(w)

  // get the invoice.
	var invoice database.Invoice
	database.DB.Where("invoice_number = ?", ps.ByName("invoiceNumber")).First(&invoice)
  if invoice.ID == 0 {
    http.Error(w, "Invoice deleted!", 500)
		return
  }

  // obtain a list of products.
  var products []database.Product
  database.DB.Where("invoice_id = ?", invoice.ID).Find(&products)
  invoice.Products = products

	res, err := json.Marshal(invoice)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Write(res)
}

// delete the invoice.
func deleteInvoiceHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	setCors(w)

  // delete products.
  var deleteProduct database.Product
	database.DB.Where("invoice_number = ?", ps.ByName("invoiceNumber")).Delete(&deleteProduct)
	res, err_p := json.Marshal(deleteProduct)
	if err_p != nil {
		http.Error(w, err_p.Error(), 500)
	}

  // delete the invoice.
	var deletedInvoice database.Invoice
	database.DB.Where("invoice_number = ?", ps.ByName("invoiceNumber")).Delete(&deletedInvoice)
	res, err := json.Marshal(deletedInvoice)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	w.Write(res)
}

func main() {
	defer database.DB.Close()
  currencies.SetupCurrency()

	// add router and routes
	router := httprouter.New()
	router.GET("/", indexHandler)
  router.POST("/invoices", createInvoiceHandler)
  router.GET("/invoices", indexInvoiceHandler)
  router.GET("/invoices/:invoiceNumber", showInvoiceHandler)
  router.DELETE("/invoices/:invoiceNumber", deleteInvoiceHandler)
  router.POST("/products", createProductHandler)
  router.DELETE("/products/:productId", deleteProductHandler)
  router.OPTIONS("/*any", corsHandler)

	// add database
	_, err := database.Init()
	if err != nil {
		log.Println("connection to DB failed, aborting...")
		log.Fatal(err)
	}
	log.Println("connected to DB")

	// print env
	env := os.Getenv("APP_ENV")
	if env == "production" {
		log.Println("Running api server in production mode")
	} else {
		log.Println("Running api server in dev mode")
	}

	http.ListenAndServe(":8080", router)
}
