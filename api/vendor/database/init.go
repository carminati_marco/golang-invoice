package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"time"
	"currencies"
)

var DB *gorm.DB
var err error

type ValidityStatus struct {
    Text   string
    Code   int
		Invoice Invoice
}

type Product struct {
	gorm.Model
	Invoice Invoice
	InvoiceID int
	InvoiceNumber int
	Name string
	Price float32 `sql:"DECIMAL(10,2)"`
	Currency string
	InvoicePrice float32 `sql:"DECIMAL(10,2)"`
	Amount float32 `sql:"DECIMAL(10,2)"`
}

type Invoice struct {
	gorm.Model
	InvoiceNumber int	`gorm:"index"`
	CompanyName string
	TotalNetAmount float32
	TotalGrossAmount float32
	Currency string
	Products []Product

}

func (i Invoice) IsValid() ValidityStatus {
	// check currency.
  _, ok := currencies.Currencies[i.Currency]
  if ! ok {
		return ValidityStatus{Code: 500, Text: "Currency not found"}
  }

  // check Invoice number.
  var invoice_check Invoice
  DB.Where("invoice_number = ?", i.InvoiceNumber).First(&invoice_check)
	if invoice_check.InvoiceNumber > 0 {
		return ValidityStatus{Code: 400, Text: "Invoice exists! Check InvoiceNumber"}
	}
	return ValidityStatus{Code:200}
}

func (p Product) IsValid() ValidityStatus {
	// check currencies.
	_, ok := currencies.Currencies[p.Currency]
	if ! ok {
		return ValidityStatus{Code: 400, Text: "Currency not found"}
	}

	// check Invoice number.
	var invoice_check Invoice
	DB.Where("invoice_number = ?", p.InvoiceNumber).First(&invoice_check)
	if invoice_check.InvoiceNumber == 0 {
		return ValidityStatus{Code: 400, Text: "Invoice DOES NOT exist! Check InvoiceNumber"}
	}

	return ValidityStatus{Code:200,Invoice:invoice_check}

}

func addDatabase(dbname string) error {
	// create database with dbname, won't do anything if db already exists
	DB.Exec("CREATE DATABASE " + dbname)

	// connect to newly created DB (now has dbname param)
	connectionParams := "dbname=" + dbname + " user=docker password=docker sslmode=disable host=db"
	DB, err = gorm.Open("postgres", connectionParams)
	if err != nil {
		return err
	}

	return nil
}

func Init() (*gorm.DB, error) {
	// set up DB connection and then attempt to connect 5 times over 25 seconds
	connectionParams := "user=docker password=docker sslmode=disable host=db"
	for i := 0; i < 5; i++ {
		DB, err = gorm.Open("postgres", connectionParams) // gorm checks Ping on Open
		if err == nil {
			break
		}
		time.Sleep(5 * time.Second)
	}

	if err != nil {
		return DB, err
	}

	// create table if it does not exist
	if !DB.HasTable(&Invoice{}) {
		DB.CreateTable(&Invoice{})
	}

	if !DB.HasTable(&Product{}) {
		DB.CreateTable(&Product{})
	}

	// create test invoice + product.
	//testInvoice := Invoice{InvoiceNumber: 1234, CompanyName: "MarcoLang", Currency: "EUR"}
	//DB.Create(&testInvoice)

	//testProduct := Product{InvoiceNumber: 1234, Name: "Product", Price: 12.45, Currency: "EUR"}
	//DB.Create(&testProduct)

	return DB, err
}
