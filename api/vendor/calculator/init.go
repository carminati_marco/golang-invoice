package calculator

import (
  "currencies"
  "database"
)

const VAT int = 20
const GROSS float32 = float32(100 + VAT)/float32(100)

func AddProduct(product database.Product) database.Product {
  // get invoice.
  var invoice = product.Invoice

  // check rate and create price
  var price float32 = 0
  if product.Currency == invoice.Currency {
    price = product.Price
  } else {
    var rate = currencies.GetRates(product.Currency, invoice.Currency)
    price = product.Price * rate
  }

  // update InvoicePrice
  product.InvoicePrice = price

  // calculate inv - TotalNetAmount (+= price)
  // increase - TotalNetAmount (-= InvoicePrice) + TotalGrossAmount (TotalNetAmount * (1+VAT))
  invoice.TotalNetAmount = invoice.TotalNetAmount + price
  invoice.TotalGrossAmount = invoice.TotalNetAmount  * GROSS

  // save invoice.
	database.DB.Save(&invoice)

  // update product.
  product.Invoice = invoice
  database.DB.Save(&product)
  return product
}

func RemoveProduct(product database.Product) database.Product {

  // get the invoice
  var invoice database.Invoice
	database.DB.Where("ID = ?", product.InvoiceID).First(&invoice)

  // decrease - TotalNetAmount (-= InvoicePrice) + TotalGrossAmount (TotalNetAmount * (1+VAT))
  invoice.TotalNetAmount = invoice.TotalNetAmount - product.InvoicePrice
  invoice.TotalGrossAmount = invoice.TotalNetAmount * GROSS

  // save invoice + delete product
  database.DB.Save(&invoice)
  database.DB.Delete(product)

  return product
}
