package currencies

import (
  "encoding/json"
	"fmt"
  "io/ioutil"
	"log"
	"net/http"
)

var Currencies = map[string]float32{}
const FixerUrl string = "https://api.fixer.io/latest"
const FixerUrlRate string = "https://api.fixer.io/latest?base=%s&symbols=%s"

type CurrenciesAPIResponse struct {
	Base  string `json:"base"`
	Date  string `json:"date"`
	Rates map[string]float32 `json:"rates"`
}

func getCurrencies(body []byte) (*CurrenciesAPIResponse, error) {
    var s = new(CurrenciesAPIResponse)
    err := json.Unmarshal(body, &s)
    if(err != nil) {
      log.Println("connection to DB failed, aborting...")
  		log.Fatal(err)
    }
    return s, err
}

func SetupCurrency() {
  // create a Currencies object to obtain accepted currency code in fixer.io
  // alternative: https://godoc.org/golang.org/x/text/currency

  // get all currencies
  response, err := http.Get(FixerUrl)
  if err != nil {
      fmt.Printf("The HTTP request failed with error %s\n", err)
  } else {
      // read response.boty + CAST with CurrenciesAPIResponse.
      body, _ := ioutil.ReadAll(response.Body)
      s, err := getCurrencies([]byte(body))

      if(err != nil){
        log.Println("CurrenciesAPIResponse failed, aborting...")
    		log.Fatal(err)
      }
      // create Currencies maps.
      Currencies[s.Base] = 1
      for k, v := range s.Rates {
          Currencies[k] = v
      }
  }
}

func GetRates(base string, symbols string) float32 {
  // Obtain rate value giving 2 currencies.

  // compose API url and call.
  s := fmt.Sprintf(FixerUrlRate, base, symbols)
  response, err := http.Get(s)
  if err != nil {
      fmt.Printf("The HTTP request failed with error %s\n", err)
  } else {

      // read response.boty + CAST with CurrenciesAPIResponse.
      body, _ := ioutil.ReadAll(response.Body)
      s, err := getCurrencies([]byte(body))

      if(err != nil){
        log.Println("CurrenciesAPIResponse failed, aborting...")
        log.Fatal(err)
      }

      // iter to find the Rates.
      for k, v := range s.Rates {
          if k == symbols {
            return v
          }
      }
      log.Println("Rate not found")
  }
  return -1

}
