# README #

A RESTful API with endpoints for dummy accounting application.

### Who do I talk to? ###

* marco carminati, carminati.marco@gmail.com

# main #

This is the first project made in golang.

## How to install  ##

After download the project, set the APP_ENV and compose the DockerFile.

```bash
$ export APP_ENV=dev
$ docker-compose up --build
```

Postgres and Golang containers will be created.
NOTE: Golang uses the port 5000

## The endpoints  ##
#### Create new Invoice ####
```bash
curl -X POST --data '{"InvoiceNumber": 345, "CompanyName": "MarcoLang", "Currency": "EUR"}' http://127.0.0.1:5000/invoices
```
Response: the created Invoice.
```javascript
{"ID":115,"CreatedAt":"2018-02-18T16:38:24.3012496Z","UpdatedAt":"2018-02-18T16:38:24.3012496Z","DeletedAt":null,"InvoiceNumber":345,"CompanyName":"MarcoLang","TotalNetAmount":0,"TotalGrossAmount":0,"Currency":"EUR","Products":null
```
#### Get list of created Invoices ####
```bash
curl -X GET http://127.0.0.1:5000/invoices
```
Response: A list of Invoices
#### Get single Invoice ####
```bash
curl -X GET http://127.0.0.1:5000/invoices/:InvoiceNumber
example: curl -X GET http://127.0.0.1:5000/invoices/345
```
Response: Invoice with the relative Product List
NOTE: We use InvoiceNumber as parameter; It's easier to use endpoints.
```javascript
{"ID":115,"CreatedAt":"2018-02-18T16:38:24.30125Z","UpdatedAt":"2018-02-18T16:38:24.30125Z","DeletedAt":null,"InvoiceNumber":345,"CompanyName":"MarcoLang","TotalNetAmount":0,"TotalGrossAmount":0,"Currency":"EUR","Products":[]}
```
#### Delete Invoice ####
```bash
curl -X DELETE http://127.0.0.1:5000/invoices/:InvoiceNumber
curl -X DELETE http://127.0.0.1:5000/invoices/345
```
Response: a null Invoice object.
```javascript
{"ID":0,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"InvoiceNumber":0,"CompanyName":"","TotalNetAmount":0,"TotalGrossAmount":0,"Currency":"","Products":null}
```
#### Create Product ####
```javascript
curl -X POST --data '{"InvoiceNumber": 345, "Name": "Prodotto", "Price": 10, "Currency": "EUR" }' http://127.0.0.1:5000/products
```
Response: The Product creteated and relative updated Invoice
```javascript
{"ID":123,"CreatedAt":"2018-02-18T16:47:42.9299965Z","UpdatedAt":"2018-02-18T16:47:42.9396176Z","DeletedAt":null,"Invoice":{"ID":116,"CreatedAt":"2018-02-18T16:46:22.183654Z","UpdatedAt":"2018-02-18T16:47:42.9368848Z","DeletedAt":null,"InvoiceNumber":345,"CompanyName":"MarcoLang","TotalNetAmount":10,"TotalGrossAmount":12,"Currency":"EUR","Products":null},"InvoiceID":116,"InvoiceNumber":345,"Name":"Prodotto","Price":10,"Currency":"EUR","InvoicePrice":10,"Amount":0}
```
#### Delete Product ####
```bash
curl -X DELETE http://127.0.0.1:5000/products/:productID
example: curl -X DELETE http://127.0.0.1:5000/products/123
```
Response: a blank Product
```javascript
{"ID":0,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"Invoice":{"ID":0,"CreatedAt":"0001-01-01T00:00:00Z","UpdatedAt":"0001-01-01T00:00:00Z","DeletedAt":null,"InvoiceNumber":0,"CompanyName":"","TotalNetAmount":0,"TotalGrossAmount":0,"Currency":"","Products":null},"InvoiceID":0,"InvoiceNumber":0,"Name":"","Price":0,"Currency":"","InvoicePrice":0,"Amount":0}
```

### Next step ###
1. To integrate Unit Test for REST API
2. Code review; Move the functions are used from router in a "Controller"
3. Find a validator package instead "IsValid" funct inserted in project
